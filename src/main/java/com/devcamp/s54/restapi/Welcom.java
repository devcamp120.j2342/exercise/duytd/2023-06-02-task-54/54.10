package com.devcamp.s54.restapi;
import java.text.*;
import java.util.Date;
import org.springframework.web.bind.annotation.*;
@RestController
public class Welcom {
    @CrossOrigin
    @GetMapping("/devcamp-welcome")
    public String nice() {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Date now= new Date();
        return String.format("Have a nice day. It is %s!.", dateFormat.format(now));
    }
}
